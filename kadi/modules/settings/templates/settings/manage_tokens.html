{# Copyright 2020 Karlsruhe Institute of Technology
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License. #}

{% extends "settings/base.html" %}

{% block breadcrumbs %}
  {{ super() }}
  {% snippet "breadcrumb", url=url_for("settings.manage_tokens"), title=title %}
{% endblock %}

{% block content %}
  <div class="card">
    {% include "settings/snippets/menu.html" %}

    <div class="card-body">
      <div v-if="token">
        <p>
          <i class="fas fa-info-circle"></i>
          {% trans %}A new personal access token has been created.{% endtrans %}
          {% trans %}Please keep it somewhere safe as you will not be able to access it again.{% endtrans %}
        </p>
        <strong>{% trans %}Your personal access token{% endtrans %}</strong>
        <div class="input-group input-group-sm">
          <input class="form-control" readonly="true" :value="token">
          <div class="input-group-append">
            <clipboard-button :content="token" class="input-group-text"></clipboard-button>
          </div>
        </div>
        <hr>
      </div>
      <submission-form>
        {{ render_csrf() }}

        <div class="row">
          <text-field class="col-lg-8" :field="{{ json_field(form.name) }}"></text-field>
          <date-time-field class="col-lg-4" :field="{{ json_field(form.expires_at) }}"></date-time-field>
        </div>
        <div class="mb-4">
          <collapse-item id="scopes" :is-collapsed="scopesCollapsed">
            <strong>{% trans %}Scopes{% endtrans %}</strong>
          </collapse-item>
          <span class="text-muted ml-3">
            <i class="fas fa-info-circle"></i>
            {% trans %}Scopes can restrict a token's access to specific resources or actions.{% endtrans %}
          </span>
        </div>
        <div id="scopes" class="collapse hide mb-4">
          <div class="d-md-flex justify-content-between">
            <div class="mt-4 mt-md-0" v-for="(actions, model) in scopes" :key="model">
              <p>
                <input type="checkbox"
                       class="align-middle"
                       :id="model"
                       :checked="allScopesChecked(model)"
                       @click="checkAllScopes(model, $event)">
                <label :for="model">
                  <strong>{$ model | capitalize $}</strong>
                </label>
              </p>
              <div v-for="(obj, action) in actions" :key="`${model}.${action}`">
                <input type="checkbox"
                       class="align-middle"
                       name="scopes"
                       v-model="obj.checked"
                       :id="`${model}.${action}`"
                       :value="`${model}.${action}`">
                <label :for="`${model}.${action}`">{$ action $}</label>
              </div>
            </div>
          </div>
        </div>
        <submit-field :field="{{ json_field(form.submit) }}"></submit-field>
      </submission-form>
      <hr>
      <dynamic-pagination endpoint="{{ url_for('api.get_access_tokens') }}"
                          placeholder="{% trans %}No access tokens.{% endtrans %}"
                          :per-page="5"
                          ref="pagination">
        <template #default="props">
          <p>
            <strong>{% trans %}Access tokens{% endtrans %}</strong>
            <span class="badge badge-pill badge-light text-muted border border-muted">{$ props.total $}</span>
          </p>
          <ul class="list-group" v-if="props.total > 0">
            <li class="list-group-item py-2 bg-light">
              <div class="row align-items-center">
                <div class="col-md-2">{% trans %}Name{% endtrans %}</div>
                <div class="col-md-3">{% trans %}Created at{% endtrans %}</div>
                <div class="col-md-3">{% trans %}Expires at{% endtrans %}</div>
                <div class="col-md-3">{% trans %}Scopes{% endtrans %}</div>
              </div>
            </li>
            <li class="list-group-item" v-for="token in props.items" :key="token.id">
              <div class="row align-items-center mb-2">
                <div class="col-md-2">
                  <strong :class="{'crossed': token.is_expired}">{$ token.name $}</strong>
                  <br>
                  <span class="text-danger" v-if="token.is_expired">{% trans %}Expired{% endtrans %}</span>
                </div>
                <div class="col-md-3">
                  <local-timestamp :timestamp="token.created_at"></local-timestamp>
                  <br>
                  <small class="text-muted">
                    (<from-now :timestamp="token.created_at"></from-now>)
                  </small>
                </div>
                <div class="col-md-3">
                  <div v-if="token.expires_at">
                    <local-timestamp :timestamp="token.expires_at" format="LL"></local-timestamp>
                    <br>
                    <small class="text-muted">
                      (<from-now :timestamp="token.expires_at"></from-now>)
                    </small>
                  </div>
                  <span v-if="!token.expires_at">{% trans %}Never{% endtrans %}</span>
                </div>
                <div class="col-md-3">
                  <div v-if="token.scopes.length > 0">
                    <span v-for="(scope, index) in token.scopes" :key="`${scope.object}.${scope.action}`">
                      {$ `${scope.object}.${scope.action}` $}<span v-if="index < token.scopes.length - 1">,</span>
                    </span>
                  </div>
                  <span v-if="token.scopes.length === 0">{% trans %}Full access{% endtrans %}</span>
                </div>
                <div class="col-md-1 d-md-flex justify-content-end">
                  <button class="btn btn-sm btn-light" @click="removeToken(token)" :disabled="token.disabled">
                    <i class="fas fa-trash"></i>
                  </button>
                </div>
              </div>
              <small class="text-muted">
                <i class="fas fa-info-circle"></i>
                {% trans %}Last used:{% endtrans %}
                <span v-if="token.last_used === null">{% trans %}Never{% endtrans %}</span>
                <span v-else>
                  <local-timestamp :timestamp="token.last_used"></local-timestamp>
                </span>
              </small>
            </li>
          </ul>
        </template>
      </dynamic-pagination>
    </div>
  </div>
{% endblock %}

{% block scripts %}
  {{ render_js("app/settings/manage-tokens.js") }}
{% endblock %}
