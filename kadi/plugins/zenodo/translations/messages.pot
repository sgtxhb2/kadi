# Translations template for Kadi4Mat.
# Copyright (C) 2020 Karlsruhe Institute of Technology
# This file is distributed under the same license as the Kadi4Mat project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Kadi4Mat VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-11-16 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: plugins/zenodo/plugin.py:114
msgid "Unknown error"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_oauth.html:15
msgid ""
"Zenodo is a general-purpose open-access repository developed and operated"
" by CERN. It allows researchers to deposit data sets, research software, "
"reports, and any other research related digital artifacts. Connecting "
"your account to Zenodo makes it possible to directly upload records to "
"Zenodo."
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:15
msgid ""
"Note that records uploaded to Zenodo will not be published directly, i.e."
" each record can be reviewed and edited via Zenodo before publishing."
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:19
msgid "The following metadata of a record will be used for the upload to Zenodo:"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:21
msgid "Title"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:22
msgid "License"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:24
msgid "Description"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:26
msgid "Note that Zenodo only supports a limited amount of Markdown/HTML tags"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:30
msgid "Tags"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:32
msgid "Tags will be used as keywords in Zenodo"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:36
msgid "Creator"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:38
msgid ""
"The display name of the record's creator will be used as the first author"
" in Zenodo"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:42
msgid "Extras"
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:44
msgid ""
"Any extra metadata will be uploaded as a separate JSON file called "
"\"_meta.json\""
msgstr ""

#: plugins/zenodo/templates/zenodo/description_publication.html:48
msgid "All records will be uploaded as <em>Dataset</em>."
msgstr ""

#: plugins/zenodo/templates/zenodo/upload_error.html:16
msgid "Error uploading record."
msgstr ""

#: plugins/zenodo/templates/zenodo/upload_error.html:18
msgid "The following error occured while uploading the record to Zenodo:"
msgstr ""

#: plugins/zenodo/templates/zenodo/upload_success.html:16
msgid "Record uploaded successfully."
msgstr ""

#: plugins/zenodo/templates/zenodo/upload_success.html:18
msgid "To review and publish your upload via Zenodo, head to the following URL:"
msgstr ""
