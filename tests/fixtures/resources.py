# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from io import BytesIO

import pytest
from flask import current_app
from PIL import Image

from kadi.lib.api.core import create_access_token
from kadi.lib.api.models import AccessToken
from kadi.lib.revisions.core import create_revision
from kadi.lib.storage.core import create_filepath
from kadi.lib.storage.local import LocalStorage
from kadi.modules.accounts.providers import LocalProvider
from kadi.modules.collections.core import create_collection
from kadi.modules.groups.core import create_group
from kadi.modules.records.core import create_record
from kadi.modules.records.models import File
from kadi.modules.records.models import Upload
from kadi.modules.templates.core import create_template


@pytest.fixture
def new_user(db):
    """Fixture to create a new local user factory."""
    count = 0

    def _new_user(
        username=None, email=None, password=None, displayname=None, system_role=None
    ):
        nonlocal count
        count += 1

        username = username if username is not None else f"user_{count}"
        email = email if email is not None else username + "@example.com"
        password = password if password is not None else username
        displayname = displayname if displayname is not None else username

        identity = LocalProvider.register(
            username=username,
            email=email,
            password=password,
            displayname=username,
            system_role=system_role,
        )
        db.session.commit()

        return identity.user

    return _new_user


@pytest.fixture
def dummy_user(db):
    """Fixture to create a new local dummy user."""
    username = password = "dummy-user"

    identity = LocalProvider.register(
        username=username,
        password=password,
        email="dummy-user@example.com",
        displayname="Dummy user",
    )
    db.session.commit()

    return identity.user


@pytest.fixture
def new_access_token(db, dummy_user):
    """Fixture to create a new access token factory."""

    def _new_token(user=None, name=None, token=None, **kwargs):
        user = user if user is not None else dummy_user
        name = name if name is not None else "token"
        token = token if token is not None else AccessToken.new_token()

        create_access_token(name=name, user=user, token=token, **kwargs)
        db.session.commit()

        return token

    return _new_token


@pytest.fixture
def dummy_access_token(db, dummy_user):
    """Fixture to create a new dummy access token."""
    token = AccessToken.new_token()

    create_access_token(name="dummy-token", token=token, user=dummy_user)
    db.session.commit()

    return token


@pytest.fixture
def new_record(db, dummy_user):
    """Fixture to create a new record factory."""
    count = 0

    def _new_record(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        record = create_record(
            creator=creator,
            identifier=f"record_{count}",
            title=f"Record {count}",
            **kwargs,
        )
        db.session.commit()

        return record

    return _new_record


@pytest.fixture
def dummy_record(db, dummy_user):
    """Fixture to create a new dummy record."""
    record = create_record(
        creator=dummy_user, identifier="dummy-record", title="Dummy record"
    )
    db.session.commit()

    return record


@pytest.fixture
def new_file(db, dummy_record, dummy_image, dummy_user):
    """Fixture to create a new local file factory.

    The storage path to use has to be set before creating a new file.
    """
    count = 0

    def _new_file(creator=None, record=None, file_data=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record
        file_data = BytesIO(file_data) if file_data is not None else dummy_image

        file = File.create(
            creator=creator,
            record=record,
            name=f"dummy_{count}.jpg",
            size=0,
            state="active",
        )
        db.session.flush()

        storage = LocalStorage()

        filepath = create_filepath(str(file.id))
        storage.save(filepath, file_data)

        file.size = os.path.getsize(filepath)
        file.magic_mimetype = storage.get_mimetype(filepath)

        create_revision(file, user=creator)
        db.session.commit()

        return file

    return _new_file


@pytest.fixture
def dummy_file(monkeypatch, tmp_path, db, dummy_image, dummy_record, dummy_user):
    """Fixture to create a new local dummy file.

    Will automatically patch the current storage path to a temporary location. The
    contents of the file consists of the contents of the "dummy_image" fixture.
    """
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file = File.create(
        creator=dummy_user,
        record=dummy_record,
        name="dummy.jpg",
        size=0,
        mimetype="image/jpeg",
        magic_mimetype="image/jpeg",
        state="active",
    )
    db.session.flush()

    filepath = create_filepath(str(file.id))
    LocalStorage().save(filepath, dummy_image)

    file.size = os.path.getsize(filepath)

    create_revision(file, user=dummy_user)
    db.session.commit()

    return file


@pytest.fixture
def new_upload(db, dummy_record, dummy_user):
    """Fixture to create a new upload factory."""
    count = 0

    def _new_upload(creator=None, record=None, size=0, file=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record

        upload = Upload.create(
            creator=creator,
            record=dummy_record,
            name=f"dummy_{count}.txt",
            size=size,
            file=file,
            **kwargs,
        )
        db.session.commit()

        return upload

    return _new_upload


@pytest.fixture
def dummy_upload(db, dummy_record, dummy_user):
    """Fixture to create a new dummy upload."""
    upload = Upload.create(
        creator=dummy_user, record=dummy_record, name=f"dummy.txt", size=0
    )
    db.session.commit()

    return upload


@pytest.fixture
def new_collection(db, dummy_user):
    """Fixture to create a new collection factory."""
    count = 0

    def _new_collection(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        collection = create_collection(
            creator=creator,
            identifier=f"collection_{count}",
            title=f"Collection {count}",
            **kwargs,
        )
        db.session.commit()

        return collection

    return _new_collection


@pytest.fixture
def dummy_collection(db, dummy_user):
    """Fixture to create a new dummy collection."""
    collection = create_collection(
        creator=dummy_user, identifier="dummy-collection", title="Dummy collection"
    )
    db.session.commit()

    return collection


@pytest.fixture
def new_group(db, dummy_user):
    """Fixture to create a new group factory."""
    count = 0

    def _new_group(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        group = create_group(
            creator=creator,
            identifier=f"group_{count}",
            title=f"Group {count}",
            **kwargs,
        )
        db.session.commit()

        return group

    return _new_group


@pytest.fixture
def dummy_group(db, dummy_user):
    """Fixture to create a new dummy group."""
    group = create_group(
        creator=dummy_user, identifier="dummy-group", title="Dummy group"
    )
    db.session.commit()

    return group


@pytest.fixture
def new_template(db, dummy_user):
    """Fixture to create a new template factory."""
    count = 0

    def _new_template(creator=None, type="record", data=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        template = create_template(
            creator=creator,
            identifier=f"template_{count}",
            title=f"Template {count}",
            type=type,
            data=data if data is not None else {},
            **kwargs,
        )
        db.session.commit()

        return template

    return _new_template


@pytest.fixture
def dummy_template(db, dummy_user):
    """Fixture to create a new dummy template."""
    template = create_template(
        creator=dummy_user,
        identifier="dummy-template",
        title="Dummy template",
        type="record",
        data={},
    )
    db.session.commit()

    return template


@pytest.fixture
def dummy_image():
    """Fixture to create an in-memory JPEG image with a single white pixel."""
    image = Image.new("RGB", size=(1, 1), color=(255, 255, 255))

    file = BytesIO()
    image.save(file, "JPEG")
    file.seek(0)

    return file
