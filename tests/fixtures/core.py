# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from contextlib import contextmanager
from contextlib import redirect_stderr
from functools import partial

import pytest
from flask.testing import FlaskClient
from flask_login import logout_user
from flask_migrate import downgrade
from flask_migrate import upgrade

from kadi.app import create_app
from kadi.ext.db import db as database
from kadi.lib.search.elasticsearch import Elasticsearch
from kadi.lib.web import url_for
from kadi.modules.accounts.utils import login_user
from kadi.modules.permissions.utils import setup_system_role


@pytest.fixture
def getfixture(request):
    """Fixture to create a factory to get other fixture values."""

    def _getfixture(name):
        return request.getfixturevalue(name)

    return _getfixture


@pytest.fixture(scope="session")
def _app():
    """Fixture to create a new application using the "testing" environment."""
    return create_app("testing")


@pytest.fixture(autouse=True)
def _app_context(_app):
    """Fixture to push a new application context.

    Executed automatically for each test.
    """
    with _app.app_context():
        yield _app


@pytest.fixture
def request_context(_app):
    """Fixture to push a new test request context."""
    with _app.test_request_context():
        yield


@pytest.fixture
def user_context(_app, dummy_user):
    """Fixture to create a context with the current user set.

    If no user is given, the current user will be set to the dummy user.
    """

    @contextmanager
    def _user_context(user=None):
        user = user if user is not None else dummy_user

        with _app.test_request_context():
            login_user(user.identity)
            yield
            logout_user()

    return _user_context


@pytest.fixture
def user_session(client, dummy_user):
    """Fixture to create a new session with a logged in user.

    If no user is given, the logged in user will be the dummy user.
    """

    @contextmanager
    def _user_session(username=None, password=None):
        if username is None or password is None:
            username = password = dummy_user.identity.username

        with client:
            client.post(
                url_for("accounts.login_with_provider", provider="local"),
                data={"username": username, "password": password},
            )

            yield

            # We need to ignore the version in case the last request was to a versioned
            # endpoint, as the request context is persistent.
            client.get(url_for("accounts.logout", _ignore_version=True))

    return _user_session


@pytest.fixture
def client(_app):
    """Fixture to create a new client for testing requests."""
    return _app.test_client()


@pytest.fixture
def api_client(_app):
    """Fixture to create a client for testing API request.

    The client can be instantiated with an access token which will be used for all
    subsequent requests.
    """

    class _APIClient(FlaskClient):
        def __init__(self, token=None, **kwargs):
            super().__init__(_app, _app.response_class, use_cookies=False)
            self.token = token

        def __getattribute__(self, attr):
            if attr in ["get", "post", "put", "patch", "delete"]:
                return partial(
                    super().__getattribute__(attr),
                    headers={"Authorization": f"Bearer {self.token}"},
                )

            return super().__getattribute__(attr)

    return _APIClient


@pytest.fixture(scope="session")
def _db(_app):
    migrations_path = _app.config["MIGRATIONS_PATH"]

    with _app.app_context():
        with open(os.devnull, "w") as f:
            with redirect_stderr(f):
                upgrade(directory=migrations_path, revision="head")

        for role_name in _app.config["SYSTEM_ROLES"]:
            setup_system_role(role_name)

        database.session.commit()

    yield database

    with _app.app_context():
        # Close the session explicitely, otherwise the database can hang.
        database.session.close()

        with open(os.devnull, "w") as f:
            with redirect_stderr(f):
                downgrade(directory=migrations_path, revision="base")


@pytest.fixture(autouse=True)
def db(_db, monkeypatch):
    """Fixture to create and initialize a new temporary database.

    All changes will be rolled back after each test.
    """
    monkeypatch.setattr(_db.session, "commit", _db.session.flush)

    return _db


@pytest.fixture(autouse=True)
def _elasticsearch(monkeypatch):
    """Fixture to patch Elasticsearch to return empty results on search.

    Will patch the Elasticsearch instance in all relevant modules where it is used.

    Executed automatically for each test.
    """

    class _Elasticsearch(Elasticsearch):
        def search(self, *args, **kwargs):
            # pylint: disable=missing-function-docstring
            return {"hits": {"hits": {}}}

    monkeypatch.setattr("kadi.lib.search.core.es", _Elasticsearch())
