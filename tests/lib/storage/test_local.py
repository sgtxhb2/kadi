# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import hashlib
import os
from io import BytesIO

import pytest

from kadi.lib.exceptions import KadiChecksumMismatchError
from kadi.lib.exceptions import KadiFilesizeExceededError
from kadi.lib.exceptions import KadiFilesizeMismatchError
from kadi.lib.storage.local import LocalStorage


def test_filepath_from_name():
    """Test if creating a filepath from a name works correctly."""
    assert LocalStorage.filepath_from_name("abcdefghij") == "ab/cd/ef/ghij"


def test_remove_empty_parent_dirs(tmp_path):
    """Test if removing empty parent directories works correctly."""
    path = os.path.join(tmp_path, "0", "1", "2", "3")
    os.makedirs(path, exist_ok=True)

    LocalStorage.remove_empty_parent_dirs(path, num_dirs=2)
    assert os.path.isdir(path)

    # Remove the child directory.
    os.rmdir(path)
    # Remove the empty parent directories.
    LocalStorage.remove_empty_parent_dirs(path, num_dirs=2)

    parent_path = os.path.dirname(path)
    assert not os.path.isdir(parent_path)

    parent_path = os.path.dirname(parent_path)
    assert not os.path.isdir(parent_path)

    parent_path = os.path.dirname(parent_path)
    assert os.path.isdir(parent_path)


def test_local_storage(tmp_path):
    """Test if the local storage works correctly."""
    file_data = 10 * b"x"
    storage = LocalStorage(max_size=len(file_data))
    filepath = os.path.join(tmp_path, "test.txt")
    tmp_filepath = os.path.join(tmp_path, "tmp.txt")

    # Try saving too much data.
    with pytest.raises(KadiFilesizeExceededError):
        storage.save(filepath, BytesIO(file_data + b"x"))

    # Try saving and copying some data.
    storage.save(filepath, BytesIO(file_data[: len(file_data) // 2]))
    storage.save(tmp_filepath, BytesIO(file_data[: len(file_data) // 2]))
    storage.save(tmp_filepath, filepath, append=True)

    # Try checking if some files exist.
    storage.exists(filepath)
    storage.exists(tmp_filepath)

    # Try moving a file
    storage.move(tmp_filepath, filepath)

    # Try opening and reading from a file.
    file = storage.open(filepath)
    assert file.read() == file_data

    # Try closing a file.
    storage.close(file)
    assert file.closed

    # Try determining the MIME type of a file.
    assert storage.get_mimetype(filepath) == "text/plain"

    # Try determining and validating the size of a file.
    assert storage.get_size(filepath) == len(file_data)

    storage.validate_size(filepath, len(file_data), "==")
    with pytest.raises(KadiFilesizeMismatchError):
        storage.validate_size(filepath, len(file_data), "!=")

    # Try determining and validating the checksum of a file.
    checksum = hashlib.md5(file_data).hexdigest()
    assert storage.get_checksum(filepath) == checksum

    storage.verify_checksum(filepath, checksum)
    with pytest.raises(KadiChecksumMismatchError):
        storage.verify_checksum(filepath, "test")

    # Try deleting a file.
    storage.delete(filepath)
    assert not os.path.isfile(filepath)
