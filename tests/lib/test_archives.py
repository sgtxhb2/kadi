# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import zipfile
from io import BytesIO

from kadi.lib.archives import create_archive
from kadi.lib.archives import get_archive_contents


def test_get_archive_contents():
    """Test if determining the contents of an archive works correctly."""
    zip_buffer = BytesIO()
    filename = "test.txt"
    file_content = "test"

    with zipfile.ZipFile(zip_buffer, mode="w", compression=zipfile.ZIP_DEFLATED) as f:
        f.writestr(filename, file_content)

    assert get_archive_contents(zip_buffer, "application/zip") == [
        {"name": filename, "size": len(file_content), "is_dir": False}
    ]


def test_create_archive(tmp_path):
    """Test if creating an archive works correctly."""
    archive_name = "test.zip"
    archive_path = os.path.join(tmp_path, archive_name)

    filename = "test.txt"
    filepath = os.path.join(tmp_path, filename)
    file_content = "test"

    with open(filepath, "w") as f:
        f.write(file_content)

    entries = [{"path": filepath, "name": filename, "size": len(file_content)}]

    assert create_archive(archive_path, entries)
    assert get_archive_contents(archive_path, "application/zip") == [
        {"name": filename, "size": len(file_content), "is_dir": False}
    ]
