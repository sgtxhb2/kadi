# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from flask import current_app
from flask_login import current_user

from kadi.lib.storage.misc import create_misc_uploads_path
from kadi.modules.accounts.utils import delete_user_image
from kadi.modules.accounts.utils import login_user
from kadi.modules.accounts.utils import save_user_image


def test_login_user(dummy_user, request_context):
    """Test if logging a user in works correctly."""
    assert not current_user.is_authenticated

    login_user(dummy_user.identity)

    assert current_user.is_authenticated


def test_save_user_image(monkeypatch, tmp_path, dummy_image, dummy_user):
    """Test if saving a user profile image works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    save_user_image(dummy_user, dummy_image)
    identifier = str(dummy_user.image_name)

    assert os.listdir(tmp_path)[0] == identifier[:2]
    assert os.path.isfile(create_misc_uploads_path(identifier))


def test_delete_user_image(monkeypatch, tmp_path, dummy_image, dummy_user):
    """Test if deleting a user profile image works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    save_user_image(dummy_user, dummy_image)
    delete_user_image(dummy_user)

    assert dummy_user.image_name is None
    assert not os.listdir(tmp_path)
