# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from kadi.modules.templates.models import Template
from tests.utils import check_view_response


def test_templates(client, user_session):
    """Test the "templates.templates" endpoint."""
    with user_session():
        response = client.get(url_for("templates.templates"))
        check_view_response(response)


@pytest.mark.parametrize("type", ["record", "extras"])
def test_new_template(type, client, user_session):
    """Test the "templates.new_template" endpoint."""
    endpoint = url_for("templates.new_template", type=type)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test", "title": "test"})

        check_view_response(response, status_code=302)
        assert Template.query.filter_by(identifier="test").first() is not None


def test_edit_template(client, dummy_template, user_session):
    """Test the "templates.edit_template" endpoint."""
    endpoint = url_for("templates.edit_template", id=dummy_template.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_template.identifier == "test"


def test_view_template(client, dummy_template, user_session):
    """Test the "templates.view_template" endpoint."""
    with user_session():
        response = client.get(url_for("templates.view_template", id=dummy_template.id))
        check_view_response(response)


def test_manage_permissions(
    client, dummy_group, dummy_template, new_user, user_session
):
    """Test the "templates.manage_permissions" endpoint."""
    endpoint = url_for("templates.manage_permissions", id=dummy_template.id)
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"users": [user.id], "groups": [dummy_group.id], "role": new_role},
        )

        check_view_response(response)
        assert (
            user.roles.filter_by(
                object="template", object_id=dummy_template.id, name=new_role
            ).first()
            is not None
        )
        assert (
            dummy_group.roles.filter_by(
                object="template", object_id=dummy_template.id, name=new_role
            ).first()
            is not None
        )


def test_delete_template(client, dummy_template, user_session):
    """Test the "templates.delete_template" endpoint."""
    with user_session():
        response = client.post(
            url_for("templates.delete_template", id=dummy_template.id)
        )

        check_view_response(response, status_code=302)
        assert not Template.query.all()
