# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_select_templates(client, user_session):
    """Test the internal "api.select_templates" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_templates"))
        check_api_response(response)


def test_get_templates(api_client, dummy_access_token):
    """Test the "api.get_templates" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_templates"))
    check_api_response(response)


def test_get_template_by_identifier(api_client, dummy_access_token, dummy_template):
    """Test the "api.get_template_by_identifier" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_template_by_identifier", identifier=dummy_template.identifier)
    )
    check_api_response(response)


@pytest.mark.parametrize(
    "endpoint", ["get_template", "get_template_user_roles", "get_template_group_roles"]
)
def test_get_template_endpoints(
    endpoint, api_client, dummy_access_token, dummy_template
):
    """Test the remaining "api.get_template*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_template.id)
    )
    check_api_response(response)
