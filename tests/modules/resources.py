# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.permissions.utils import add_role
from tests.utils import check_api_response


def check_api_post_subject_resource_role(
    client, endpoint, subject, obj, new_role="member"
):
    """Common function to test adding new roles to resources."""
    response = client.post(
        endpoint,
        json={
            subject.__class__.__tablename__: {"id": subject.id},
            "role": {"name": new_role},
        },
    )

    check_api_response(response, status_code=201)
    assert (
        subject.roles.filter_by(
            object=obj.__class__.__tablename__, object_id=obj.id, name=new_role
        ).first()
        is not None
    )


def check_api_patch_subject_resource_role(
    client,
    endpoint,
    subject,
    obj,
    change_creator_endpoint=None,
    old_role="member",
    new_role="editor",
):
    """Common function to test changing existing roles of resources."""
    add_role(subject, obj.__class__.__tablename__, obj.id, old_role)

    # Check that changing the creator role does not work, if applicable.
    if change_creator_endpoint is not None:
        response = client.patch(change_creator_endpoint, json={"name": new_role})
        check_api_response(response, status_code=409)

    response = client.patch(endpoint, json={"name": new_role})

    check_api_response(response)
    assert (
        subject.roles.filter_by(
            object=obj.__class__.__tablename__, object_id=obj.id, name=new_role
        ).first()
        is not None
    )


def check_api_delete_subject_resource_role(
    client, endpoint, subject, obj, remove_creator_endpoint=None, role="member"
):
    """Common function to test changing existing roles of resources."""
    add_role(subject, obj.__class__.__tablename__, obj.id, role)

    # Check that changing the creator role does not work, if applicable.
    if remove_creator_endpoint is not None:
        response = client.delete(remove_creator_endpoint)
        check_api_response(response, status_code=409)

    response = client.delete(endpoint)

    check_api_response(response, status_code=204)
    assert (
        subject.roles.filter_by(
            object=obj.__class__.__tablename__, object_id=obj.id
        ).first()
        is None
    )
