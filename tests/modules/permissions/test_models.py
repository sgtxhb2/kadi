# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.modules.permissions.models import Permission
from kadi.modules.permissions.models import Role


def test_role_validate_permission():
    """Test if permissions of roles are validated correctly."""
    system_role = Role.create(name="test")
    test_role = Role.create(name="test", object="test", object_id=1)

    permission = Permission.create(action="test", object="test", object_id=1)
    system_role.permissions.append(permission)
    test_role.permissions.append(permission)

    permission = Permission.create(action="test", object="test", object_id=2)
    system_role.permissions.append(permission)

    with pytest.raises(AssertionError):
        test_role.permissions.append(permission)

    permission = Permission.create(action="test", object="test")
    system_role.permissions.append(permission)

    with pytest.raises(AssertionError):
        test_role.permissions.append(permission)
