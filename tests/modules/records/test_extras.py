# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import math
from datetime import datetime
from datetime import timezone

import pytest
from marshmallow import ValidationError
from werkzeug.datastructures import MultiDict

from kadi.modules.records.extras import ExtraSchema
from kadi.modules.records.extras import parse_extra_formdata


def test_extras_jsonb(dummy_record):
    """Check if the custom JSONB type for extras works correctly."""
    dummy_record.extras = [{"key": "test", "type": "float", "value": 1e100}]
    assert isinstance(dummy_record.extras[0]["value"], float)


@pytest.mark.parametrize(
    "data,extra",
    [
        ({"key": "test", "type": "str"}, {"key": "test", "type": "str", "value": None}),
        ({"key": "test", "type": "str", "value": None}, None),
        ({"key": "test", "type": "str", "value": "test"}, None),
        (
            {"key": "test", "type": "str", "value": " "},
            {"key": "test", "type": "str", "value": None},
        ),
        (
            {"key": "test", "type": "int", "value": 123},
            {"key": "test", "type": "int", "value": 123, "unit": None},
        ),
        ({"key": "test", "type": "float", "value": 1e123, "unit": "cm"}, None),
        (
            {"key": "test", "type": "date", "value": "2020-01-01T00:00:00.000Z"},
            {"key": "test", "type": "date", "value": "2020-01-01T00:00:00+00:00"},
        ),
        (
            {
                "key": "test",
                "type": "date",
                "value": datetime.strptime(
                    "2020-01-01T00:00:00.000Z", "%Y-%m-%dT%H:%M:%S.%fZ"
                ).replace(tzinfo=timezone.utc),
            },
            {"key": "test", "type": "date", "value": "2020-01-01T00:00:00+00:00"},
        ),
        ({"key": "test", "type": "bool", "value": True}, None),
        (
            {
                "key": "test",
                "type": "dict",
                "value": [{"key": "test", "type": "str", "value": "test"}],
            },
            None,
        ),
        (
            {
                "key": "test",
                "type": "list",
                "value": [{"type": "str", "value": "test"}],
            },
            None,
        ),
    ],
)
def test_extra_schema_success(data, extra):
    """Test if the extra schema validates valid extras correctly."""
    if extra is None:
        extra = data

    assert ExtraSchema().load(data) == extra


@pytest.mark.parametrize(
    "data",
    [
        {"type": "str", "value": "test"},
        {"key": " ", "type": "str"},
        {"key": "test", "type": " "},
        {"key": "test", "type": "test"},
        {"key": "str", "value": "test"},
        {"key": "str", "type": "str", "value": "test", "unit": "cm"},
        {"key": "str", "type": "str", "value": "test", "test": "test"},
        {"key": "test", "type": "str", "value": 123},
        {"key": "test", "type": "int", "value": 123.0},
        {"key": "test", "type": "float", "value": "test"},
        {"key": "test", "type": "float", "value": math.inf},
        {"key": "test", "type": "float", "value": math.nan},
        {"key": "test", "type": "date", "value": "test"},
        {"key": "test", "type": "bool", "value": "true"},
        {"key": "test", "type": "dict", "value": {}},
        {
            "key": "test",
            "type": "dict",
            "value": [{"key": "test", "type": "str"}, {"key": "test", "type": "str"}],
        },
        {"key": "test", "type": "list", "value": [{"key": "test"}]},
    ],
)
def test_extra_schema_error(data):
    """Test if the extra schema validates invalid extras correctly."""
    with pytest.raises(ValidationError):
        ExtraSchema().load(data)


@pytest.mark.parametrize(
    "formdata,extra",
    [
        (
            [
                ("extra_key", "test"),
                ("extra_type", "str"),
                ("extra_value", ""),
                ("extra_unit", "cm"),
                ("extra_depth", "0"),
            ],
            {"key": "test", "type": "str", "value": None},
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_type", "int"),
                ("extra_value", "123"),
                ("extra_unit", "cm"),
                ("extra_depth", "0"),
            ],
            {"key": "test", "type": "int", "value": 123, "unit": "cm"},
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_type", "float"),
                ("extra_value", "1e123"),
                ("extra_unit", ""),
                ("extra_depth", "0"),
            ],
            {"key": "test", "type": "float", "value": 1e123, "unit": None},
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_type", "bool"),
                ("extra_value", "true"),
                ("extra_unit", "cm"),
                ("extra_depth", "0"),
            ],
            {"key": "test", "type": "bool", "value": True},
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_type", "date"),
                ("extra_value", "2020-01-01T00:00:00.000Z"),
                ("extra_unit", "cm"),
                ("extra_depth", "0"),
            ],
            {"key": "test", "type": "date", "value": "2020-01-01T00:00:00+00:00"},
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_key", "test"),
                ("extra_type", "dict"),
                ("extra_type", "str"),
                ("extra_value", ""),
                ("extra_value", "test"),
                ("extra_unit", ""),
                ("extra_unit", ""),
                ("extra_depth", "0"),
                ("extra_depth", "1"),
            ],
            {
                "key": "test",
                "type": "dict",
                "value": [{"key": "test", "type": "str", "value": "test"}],
            },
        ),
        (
            [
                ("extra_key", "test"),
                ("extra_key", "test"),
                ("extra_type", "list"),
                ("extra_type", "str"),
                ("extra_value", ""),
                ("extra_value", "test"),
                ("extra_unit", ""),
                ("extra_unit", ""),
                ("extra_depth", "0"),
                ("extra_depth", "1"),
            ],
            {
                "key": "test",
                "type": "list",
                "value": [{"type": "str", "value": "test"}],
            },
        ),
    ],
)
def test_parse_extra_formdata_success(formdata, extra):
    """Test if valid extra formdata is parsed correctly."""
    parsed_extras = parse_extra_formdata(formdata=MultiDict(formdata))

    assert parsed_extras.is_valid
    assert parsed_extras.values == [extra]


@pytest.mark.parametrize(
    "formdata",
    [
        [
            ("extra_key", "test"),
            ("extra_type", "test"),
            ("extra_value", "test"),
            ("extra_unit", "cm"),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", ""),
            ("extra_type", "test"),
            ("extra_value", "test"),
            ("extra_unit", "cm"),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", "test"),
            ("extra_type", "int"),
            ("extra_value", "test"),
            ("extra_unit", "cm"),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", "test"),
            ("extra_type", "float"),
            ("extra_value", "test"),
            ("extra_unit", ""),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", "test"),
            ("extra_type", "bool"),
            ("extra_value", "test"),
            ("extra_unit", "cm"),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", "test"),
            ("extra_type", "date"),
            ("extra_value", "test"),
            ("extra_unit", "cm"),
            ("extra_depth", "0"),
        ],
        [
            ("extra_key", "test"),
            ("extra_key", "test"),
            ("extra_type", "dict"),
            ("extra_type", "str"),
            ("extra_value", ""),
            ("extra_value", "test"),
            ("extra_unit", ""),
            ("extra_unit", ""),
            ("extra_depth", "0"),
            ("extra_depth", "0"),
        ],
    ],
)
def test_parse_extra_formdata_error(formdata):
    """Test if invalid extra formdata is parsed correctly."""
    parsed_extras = parse_extra_formdata(formdata=MultiDict(formdata))
    assert not parsed_extras.is_valid
