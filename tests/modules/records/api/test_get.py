# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.storage.core import create_filepath
from kadi.lib.storage.local import LocalStorage
from kadi.lib.web import url_for
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import TemporaryFile
from tests.modules.records.utils import initiate_upload
from tests.modules.records.utils import upload_chunk
from tests.utils import check_api_response


@pytest.mark.parametrize("export_type", ["json"])
def test_download_record_export(export_type, client, dummy_record, user_session):
    """Test the internal "api.download_record_export" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.download_record_export",
                id=dummy_record.id,
                export_type=export_type,
            )
        )
        check_api_response(response)


def test_select_records(client, user_session):
    """Test the internal "api.select_records" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_records"))
        check_api_response(response)


def test_select_record_types(client, user_session):
    """Test the internal "api.select_record_types" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_record_types"))
        check_api_response(response)


def test_get_file_preview(client, dummy_file, dummy_record, user_session):
    """Test the internal "api.get_file_preview" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_file_preview", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_api_response(response)


def test_preview_file(client, dummy_file, dummy_record, user_session):
    """Test the internal "api.preview_file" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.preview_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_api_response(response, content_type="image/jpeg")


def test_download_temporary_file(
    client, db, dummy_file, dummy_record, dummy_user, user_session
):
    """Test the internal "api.download_temporary_file" endpoint."""
    temporary_file = TemporaryFile.create(
        record=dummy_record,
        creator=dummy_user,
        name=dummy_file.name,
        size=dummy_file.size,
        mimetype=dummy_file.mimetype,
        state="active",
    )
    db.session.commit()

    dst = create_filepath(str(temporary_file.id))
    src = create_filepath(str(dummy_file.id))
    LocalStorage().save(dst, src)

    with user_session():
        response = client.get(
            url_for(
                "api.download_temporary_file",
                record_id=dummy_record.id,
                temporary_file_id=temporary_file.id,
            )
        )
        check_api_response(response, content_type="image/jpeg")


def test_get_records(api_client, dummy_access_token):
    """Test the "api.get_records" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_records"))
    check_api_response(response)


def test_get_record_by_identifier(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_record_by_identifier" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_record_by_identifier", identifier=dummy_record.identifier)
    )
    check_api_response(response)


def test_get_record_link(api_client, db, dummy_access_token, dummy_record, new_record):
    """Test the "api.get_record_link" endpoint."""
    record = new_record()
    record_link = RecordLink.create(
        name="test", record_from=dummy_record, record_to=record
    )
    db.session.commit()

    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_record_link", record_id=dummy_record.id, link_id=record_link.id
        )
    )
    check_api_response(response)


def test_get_record_revision(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_record_revision" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_record_revision",
            record_id=dummy_record.id,
            revision_id=dummy_record.revisions[0].id,
        )
    )
    check_api_response(response)


@pytest.mark.parametrize(
    "endpoint,args",
    [
        ("get_record", {}),
        ("get_record_links", {"direction": "to"}),
        ("get_record_links", {"direction": "from"}),
        ("get_record_collections", {}),
        ("get_record_user_roles", {}),
        ("get_record_group_roles", {}),
        ("get_record_revisions", {}),
    ],
)
def test_get_record_endpoints(
    endpoint, args, api_client, dummy_access_token, dummy_record
):
    """Test the remaining "api.get_record*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_record.id, **args)
    )
    check_api_response(response)


def test_get_files(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_files" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_files", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_file(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )
    check_api_response(response)


def test_get_file_by_name(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file_by_name" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_file_by_name", record_id=dummy_record.id, filename=dummy_file.name
        )
    )
    check_api_response(response)


def test_download_file(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.download_file" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.download_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )
    check_api_response(response, content_type="image/jpeg")


def test_get_file_revisions(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_file_revisions" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_file_revisions", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_file_revision(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file_revision" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_file_revision",
            record_id=dummy_record.id,
            revision_id=dummy_file.revisions[0].id,
        )
    )
    check_api_response(response)


def test_get_uploads(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_uploads" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_uploads", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_upload_status(
    monkeypatch, tmp_path, api_client, dummy_access_token, dummy_record
):
    """Test the "api.get_upload_status" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    client = api_client(dummy_access_token)
    file_data = 10 * b"x"

    response = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id), file_data=file_data
    )
    response = client.get(
        url_for(
            "api.get_upload_status",
            record_id=dummy_record.id,
            upload_id=response.get_json()["id"],
        )
    )
    data = response.get_json()

    check_api_response(response)
    assert "_meta" not in data

    for index in range(data["chunk_count"]):
        chunk_size = current_app.config["UPLOAD_CHUNK_SIZE"]
        chunk_data = file_data[index * chunk_size : (index + 1) * chunk_size]

        upload_chunk(
            client, data["_actions"]["upload_chunk"], chunk_data=chunk_data, index=index
        )

    client.post(data["_actions"]["finish_upload"])
    response = client.get(data["_links"]["status"])
    data = response.get_json()

    check_api_response(response)
    assert "_meta" in data and "file" in data["_meta"]
