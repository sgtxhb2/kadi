# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from kadi.modules.records.models import Record
from tests.utils import check_view_response


def test_records(client, user_session):
    """Test the "records.records" endpoint."""
    with user_session():
        response = client.get(url_for("records.records"))
        check_view_response(response)


def test_new_record(client, user_session):
    """Test the "records.new_record" endpoint."""
    endpoint = url_for("records.new_record")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"identifier": "test", "title": "test", "visibility": "private"},
        )

        check_view_response(response, status_code=302)
        assert Record.query.filter_by(identifier="test").first() is not None


def test_edit_record(client, dummy_record, user_session):
    """Test the "records.edit_record" endpoint."""
    endpoint = url_for("records.edit_record", id=dummy_record.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_record.identifier == "test"


def test_view_record(client, dummy_record, user_session):
    """Test the "records.view_record" endpoint."""
    with user_session():
        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response)


@pytest.mark.parametrize("export_type", ["json"])
def test_export_record(export_type, client, dummy_record, user_session):
    """Test the "records.export_record" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.export_record", id=dummy_record.id, export_type=export_type
            )
        )
        check_view_response(response)


def test_publish_record(client, dummy_record, user_session):
    """Test the "records.publish_record" endpoint."""
    with user_session():
        response = client.get(
            url_for("records.publish_record", id=dummy_record.id, provider="test")
        )
        check_view_response(response, status_code=404)

        response = client.post(
            url_for("records.publish_record", id=dummy_record.id, provider="test")
        )
        check_view_response(response, status_code=404)


def test_manage_links_records(
    client, dummy_collection, dummy_group, dummy_record, new_record, user_session
):
    """Test the "records" tab of the "records.manage_links" endpoint."""
    endpoint = url_for("records.manage_links", id=dummy_record.id, tab="records")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        record = new_record()
        response = client.post(endpoint, data={"name": "test", "record": record.id})

        check_view_response(response, status_code=302)
        assert dummy_record.links_to[0].record_to.id == record.id


def test_manage_links_collections(
    client, dummy_collection, dummy_group, dummy_record, new_record, user_session
):
    """Test the "collections" tab of the "records.manage_links" endpoint."""
    endpoint = url_for("records.manage_links", id=dummy_record.id, tab="resources")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"collections": [dummy_collection.id]})

        check_view_response(response)
        assert dummy_record.collections[0].id == dummy_collection.id


def test_manage_permissions(client, dummy_group, dummy_record, new_user, user_session):
    """Test the "records.manage_permissions" endpoint."""
    endpoint = url_for("records.manage_permissions", id=dummy_record.id)
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"users": [user.id], "groups": [dummy_group.id], "role": new_role},
        )

        check_view_response(response)
        assert (
            user.roles.filter_by(
                object="record", object_id=dummy_record.id, name=new_role
            ).first()
            is not None
        )
        assert (
            dummy_group.roles.filter_by(
                object="record", object_id=dummy_record.id, name=new_role
            ).first()
            is not None
        )


def test_add_files(client, dummy_record, user_session):
    """Test the "records.add_files" endpoint."""
    with user_session():
        response = client.get(url_for("records.add_files", id=dummy_record.id))
        check_view_response(response)


def test_view_record_revision(client, dummy_record, user_session):
    """Test the "records.view_record_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_record_revision",
                record_id=dummy_record.id,
                revision_id=dummy_record.revisions[0].id,
            )
        )
        check_view_response(response)


def test_view_file_revision(client, dummy_file, dummy_record, user_session):
    """Test the "records.view_file_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_file_revision",
                record_id=dummy_record.id,
                revision_id=dummy_file.revisions[0].id,
            )
        )
        check_view_response(response)


def test_delete_record(client, dummy_record, user_session):
    """Test the "records.delete_record" endpoint."""
    with user_session():
        response = client.post(url_for("records.delete_record", id=dummy_record.id))

        check_view_response(response, status_code=302)
        assert dummy_record.state == "deleted"


def test_view_file(client, dummy_file, dummy_record, user_session):
    """Test the "records.view_file" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_view_response(response)


def test_edit_file(client, dummy_file, dummy_record, user_session):
    """Test the "records.edit_file" endpoint."""
    endpoint = url_for(
        "records.edit_file", record_id=dummy_record.id, file_id=dummy_file.id
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test.txt"})

        check_view_response(response, status_code=302)
        assert dummy_file.name == "test.txt"


def test_delete_file(client, dummy_file, dummy_record, user_session):
    """Test the "records.delete_file" endpoint."""
    with user_session():
        response = client.post(
            url_for(
                "records.delete_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )

        check_view_response(response, status_code=302)
        assert dummy_file.state == "inactive"
