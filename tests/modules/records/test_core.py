# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.lib.tags.models import Tag
from kadi.modules.permissions.utils import add_role
from kadi.modules.records.core import create_record
from kadi.modules.records.core import delete_record
from kadi.modules.records.core import purge_record
from kadi.modules.records.core import restore_record
from kadi.modules.records.core import update_record
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import TemporaryFile


def test_create_record(dummy_user):
    """Test if records are created correctly."""
    record = create_record(
        creator=dummy_user,
        identifier="test",
        title="test",
        type="test",
        description="# test",
        license="invalid",
        extras=[{"key": "sample_key", "value": "sample_value", "type": "str"}],
        tags=["test"],
        visibility="private",
    )

    assert Record.query.filter_by(identifier="test").first().id == record.id
    assert record.plain_description == "test"
    assert record.license is None
    assert record.revisions.count() == 1
    assert Tag.query.filter_by(name="test").first() is not None


def test_update_record(dummy_record, user_context):
    """Test if records are updated correctly."""
    with user_context():
        update_record(
            dummy_record, description="# test", license="invalid", tags=["test"]
        )

        assert dummy_record.plain_description == "test"
        assert dummy_record.license is None
        assert dummy_record.revisions.count() == 2
        assert Tag.query.filter_by(name="test").first() is not None


def test_delete_record(dummy_record, user_context):
    """Test if records are deleted correctly."""
    with user_context():
        delete_record(dummy_record)

        assert dummy_record.state == "deleted"
        assert dummy_record.revisions.count() == 2


def test_restore_record(dummy_record, user_context):
    """Test if records are restored correctly."""
    with user_context():
        delete_record(dummy_record)
        restore_record(dummy_record)

        assert dummy_record.state == "active"
        assert dummy_record.revisions.count() == 3


def test_purge_record(
    db,
    dummy_collection,
    dummy_file,
    dummy_group,
    dummy_record,
    dummy_upload,
    dummy_user,
    new_user,
    user_context,
    new_record,
):
    """Test if records are purged correctly."""
    with user_context():
        user = new_user()
        record = new_record()

        TemporaryFile.create(
            record=dummy_record, creator=dummy_user, name="test.txt", size=0
        )
        RecordLink.create(name="test", record_from=dummy_record, record_to=record)
        add_link(dummy_record.collections, dummy_collection)
        add_role(user, "record", dummy_record.id, "member")

        purge_record(dummy_record)
        db.session.commit()

        assert Record.query.get(dummy_record.id) is None
        # Only the system role should remain.
        assert user.roles.count() == 1
