Latest (1.0)
============

This section documents the latest version of the HTTP API, currently being
version ``1.0``.

.. toctree::
   :maxdepth: 1

   Records <records>
   Collections <collections>
   Groups <groups>
   Users <users>
   Templates <templates>
   Miscellaneous <misc>
