.. _apiref-modules:

Modules
=======

This section contains all API references of the :mod:`kadi.modules` package
that were not already listed in other sections.

Accounts
--------

.. automodule:: kadi.modules.accounts.providers.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.providers.ldap
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.providers.local
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.providers.shib
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.forms
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.accounts.utils
   :members:
   :show-inheritance:

Collections
-----------

.. automodule:: kadi.modules.collections.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.collections.forms
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.collections.mappings
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.collections.schemas
   :members:
   :show-inheritance:

Groups
------

.. automodule:: kadi.modules.groups.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.groups.forms
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.groups.mappings
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.groups.schemas
   :members:
   :show-inheritance:

Main
----

.. automodule:: kadi.modules.main.tasks
   :members:
   :show-inheritance:

Notifications
-------------

.. automodule:: kadi.modules.notifications.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.notifications.mails
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.notifications.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.notifications.tasks
   :members:
   :show-inheritance:

Permissions
-----------

.. automodule:: kadi.modules.permissions.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.permissions.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.permissions.utils
   :members:
   :show-inheritance:

Records
-------

.. automodule:: kadi.modules.records.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.extras
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.files
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.forms
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.mappings
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.previews
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.tasks
   :members:
   :show-inheritance:

Settings
--------

.. automodule:: kadi.modules.settings.forms
   :members:
   :show-inheritance:

Templates
---------

.. automodule:: kadi.modules.templates.core
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.templates.forms
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.templates.schemas
   :members:
   :show-inheritance:
