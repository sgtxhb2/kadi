.. _installation-plugins:

Plugins
=======

This section describes how plugins can be installed and configured to extend
the functionality of Kadi4Mat.

Installing plugins
------------------

Plugins can either be part of the main application package or implemented as
independent Python packages, which have to be installed separately in the same
virtual environment that Kadi4Mat itself was installed in. In both cases,
plugins are configured via the ``PLUGINS`` configuration value in the Kadi
configuration file after they have been installed:

.. code-block:: python3

    PLUGINS = ["example", "another-example"]

This will activate the plugin with the name ``my-plugin`` in the application.
Some plugins might require additional configuration to work correctly or to
change how they behave. This configuration is plugin specific and can be
specified via the ``PLUGIN_CONFIG`` configuration value in the Kadi
configuration file:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "example": {
            "example_key": "example_value",
        },
    }

After configuring all plugins, the application has to be restarted for the
changes to take effect. In the future, it is planned to enable plugin
management and configuration via the web interface.

First party plugins
-------------------

Currently all first party plugins are part of the main application package and
are therefore available automatically.

Zenodo
~~~~~~

The ``zenodo`` plugin allows users to connect their Kadi4Mat account to a
`Zenodo <https://zenodo.org>`__ account in order to upload records directly to
Zenodo. The following configuration values have to be specified when using the
Zenodo plugin:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "zenodo": {
            "base_url": "https://sandbox.zenodo.org",  # Optional, defaults to "https://zenodo.org"
            "client_id": "<zenodo_client_id>",         # Required
            "client_secret": "<zenodo_client_secret>", # Required
        }
    }

The client ID and secret can be obtained directly via Zenodo by navigating to
*Applications > Developer Applications* in the account settings page. Besides
setting some metadata that will be displayed to users of Kadi4Mat, the
following settings are necessary for the integration to work correctly:

 * Redirect URIs: ``https://<base_url>/settings/services/authorize/zenodo``
 * Client type: ``Confidential``

The ``<base_url>`` in the redirect URI value is the base URL of the Kadi4Mat
instance to use, e.g. ``kadi4mat.iam-cms.kit.edu``.

For testing purposes, the `Zenodo sandbox <https://sandbox.zenodo.org>`__ can
be used instead, together with changing the ``base_url`` in the plugin
configuration, as seen in the configuration example above.
