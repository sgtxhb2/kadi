Production
==========

This section describes how to install and update Kadi4Mat in a production
environment. There are currently two ways to do this: manual installation and
installation via setup script. The former is recommended for first-time
installations, as all necessary steps are explained in more detail. When using
the installation script instead, it is still highly recommended to check the
content of the script to see what it does.

.. note::

  The described steps only cover installing and setting up the application and
  its dependencies. Keep in mind that, once deployed, there are other aspects
  to consider as well, most importantly securing the server and services (e.g.
  keeping packages up to date, using a firewall, etc.) and regularly creating
  backups of the database and files.

.. toctree::
   :maxdepth: 2

   Manual installation <manual>
   Installation via setup script <script>
   Updating the application <updating>
