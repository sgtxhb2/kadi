.. image:: /images/kadi.png
   :width: 175 px
   :align: right

Welcome to Kadi4Mat's documentation!
====================================

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, an
open source software for managing research data. The goal of this project is to
combine the ability to manage and exchange data (the *Repository*) with the
possibility to analyze, visualize and transform said data (the *Electronic Lab
Notebook*). More information can be found `here
<https://kadi.iam-cms.kit.edu/>`__.

The following chapters contain a description on how to install Kadi4Mat, either
inside a development or production environment, some general topics and useful
tools for development, the usage and endpoints of the HTTP API and also a
(mostly) complete API reference of the project itself.

.. note::

  This documentation is still a work in progress and does not cover all aspects
  of Kadi4Mat yet.

Table of contents
=================

.. toctree::
   :maxdepth: 2

   Installation <installation/index>
   Development <development/index>
   HTTP API <httpapi/index>
   API reference <apiref/index>
