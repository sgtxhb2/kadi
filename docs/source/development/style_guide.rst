.. _development-style_guide:

Style guide
===========

This section describes some general guidelines on coding style in this project.
Various :ref:`Tools <development-general-tools>` related to this task are
already used for Python and JavaScript code. It is recommended to check them
out first.

For anything that is not covered by the style guide or any formatting tool yet,
it is recommended to simply take a look at existing code, since consistency is
always the most important aspect.

Comments
--------

Comments should generally be whole sentences and start with an uppercase
letter. There should always be a single space after the initial comment
character(s), namely ``#`` for Python, ``//`` or ``/* */`` (for multiline
comments) for JavaScript and ``<!-- -->`` for HTML.

Docstrings
----------

For general conventions about docstrings in Python, take a look at `PEP 257
<https://www.python.org/dev/peps/pep-0257/>`__. As for the docstring format,
reST (reStrucuredText) style should be used, like in the following example:

.. code-block:: python3

    """Brief description in one line (directly after the triple quotes).

    Longer description and additional information, if needed. Otherwise, the
    closing quotes should be put in the same line as the docstring. Here we
    could also include an example or anything else that reST allows us to do:

    .. code-block:: python3

        foo(param_a, param_b)

    :param param_a: Description of param_a.
    :param param_b: (optional) Very long description of an optional param_b
        that may take up multiple lines.
    :return: Description of return value.
    :raises Exception: If something goes wrong.
    """

Docstrings in JavaScript should generally follow the `JSDoc
<https://jsdoc.app/>`__ format.

Naming Conventions
------------------

Python
~~~~~~

Variable names use ``snake_case``.

Class names use ``CapitalizedWords``.

Constants use ``UPPER_CASE_WITH_UNDERSCORES``.

Private functions or attributes use a ``_single_leading_underscore``.

Using builtin names as variables names (e.g. ``str``) should be avoided if
possible.

JavaScript
~~~~~~~~~~

Variable and property names generally use ``camelCase``. In some cases where
the line between frontend and backend is blurred, ``snake_case`` can be fine
too.

Class names use ``CapitalizedWords``.
