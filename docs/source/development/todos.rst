.. _development-todos:

Todos
=====

This section contains planned features or ideas that will, should or could be
implemented in the future. The features are grouped into the three following
lists:

1) Improvements and changes to the existing code or to existing features.

2) Planned features where no substantial work has been done yet or concepts
   still need to be developed.

3) Ideas and nice-to-have features/improvements to the general code base that
   could be considered in the longer term.

All items belong to one or more different topics:

* ``API`` Features related to the HTTP API
* ``Architecture`` Features related to general structural changes to the code
  architecture
* ``Authentication`` Features related to user authentication
* ``Authorization`` Features related to access management for the different
  resources, mainly for data and metadata
* ``Deployment`` Features that mostly become relevant once the application is
  deployed into a production environment and especially once a stable version
  of the application has been released
* ``Docs`` Improvements to this or other documentation
* ``Files`` Features related to the handling of data
* ``Metadata`` Features related to the handling of metadata
* ``Other`` Other features that do not have their own category (yet)
* ``Plugins`` Features related to the development of the plugin
  infrastructure itself or features that might be suitable to be developed as
  plugins
* ``Publishing`` Features related to the publishing of datasets
* ``Search`` Features related to the search functionality for the different
  resources, mainly for data and metadata
* ``UI`` Features related to the user interface, currently mostly focused on
  the web interface
* ``Workflow`` Features related to automated workflows for using different
  kinds of (possibly user-provided) data analysis, visualization and
  transformation tools

The items are currently sorted alphabetically by their first topic, not
necessarily by priority or implementation complexity. Note that some items
require bigger changes that might affect how some other features function as
well (e.g. the ability for low-level data access or integrating user-provided
tools and workflows). Furthermore, some items might relate to features that are
developed in a separate code base until those are big enough to warrant their
own lists/documentation.

Improvements
------------

* ``API`` Implement more batch operations where necessary (e.g. deleting
  multiple files) in a preferably RESTful way

* ``API`` Implement more ways to parametrize the content that is returned by
  endpoints where necessary

* ``Authentication`` ``Plugins`` More generic authentication system, so
  existing and custom authentication systems (e.g. using OIDC/OAuth2) can be
  implemented as plugins

* ``Authorization`` Improve the current way global access permissions are
  handled (e.g. by setting the visibility of a resource), but provide
  some alternative way to make resources public, at least for logged in users

* ``Authorization`` Improved access management

  * Make roles configurable if necessary (when considering only *rwx*
    permissions, static roles probably make more sense)
  * Ability to assign fine granular permissions in addition to roles
  * Better way to grant access to sub-resources (e.g. records inside
    collections)
  * Ability to define some kind of permission hierarchy, so data of inactive
    users can still be accessed if needed
  * Ability to easily change system roles of users

* ``Deployment`` Caching of expensive operations (e.g frequent and complex
  database queries or complete view functions) using an actual caching system

* ``Docs`` Incomplete/missing chapters in the documentation

  * Installation: Installation under different systems/environments,
    explanation of all configuration values and administration beyond simply
    installing the application (including the CLI and other provided tooling),
    especially for production environments (e.g. basic security considerations,
    possible backup strategies, etc.)
  * Development: Developing tests and plugins, common issues, other general
    topics
  * API reference: Documenting the frontend code and extracting the appropriate
    docstrings (e.g. using JSDoc which can also be integrated with Sphinx)

* ``Files`` More powerful quota system

  * Ability to set quotas (at least for locally stored files) per user or
    group, possibly based on some user attributes/affiliations or maybe tied to
    the system roles

* ``Files`` ``Plugins`` More generic storage functionalities

  * Ability to integrate other storage backends/systems as plugins and
    simultaneously using multiple such storage backends transparently
  * Ability to also simply link to files using an URL or path
  * Ability to make use of other protocols for uploading files (e.g.
    FTP/GridFTP)

* ``Metadata`` Improved extra record metadata schema and templating

  * Allow for more value types (e.g. OS independent paths, tables, etc.)
  * Allow to set predefined selections/restricting the possible values of keys,
    values, units, etc. as well as other validations in templates
  * Allow to use a metadata template directly as a "value" of a nested metadata
    entry
  * Allow to "highlight" some entries, appearing differently/more prominently
    e.g. on the record overview page

* ``Other`` Revised revision system

  * Allow every database change to be tracked if necessary (including more
    complex relationships)
  * Possibly extend the current system to allow for actual file data revisions
    as well
  * Add the ability for a user to tag a revision
  * Add the ability to directly view an old version of a resource and possibly
    to also restore from that version

* ``Other`` Improved logging configuration

  * Provide a central config file to configure logging levels, formats, files,
    and possible Sentry configuration as well

* ``Other`` ``API`` ``Plugins`` Improved export functionality of data and
  metadata for interoperability and archiving

  * Provide suitable endpoints for bulk export and import of data
  * Offer more formats/interfaces to export and/or package data and metadata,
    e.g. OAI-PMH interfaces, Bagit, Frictionless Data, etc.
  * Most of these export providers could be good candidates to be developed as
    plugins

* ``Other`` Performance improvements for database access (aside from caching
  strategies), especially mitigating the slowness of the ORM for larger queries

* ``Plugins`` Improved plugin system

  * Implement more hooks for plugins to extend or override core functionality,
    including templates, custom frontend assets, static files, database models
    (using branching migrations), background tasks, CLI commands, etc.
  * Add a way to manage and configure installed plugins via the web interface

* ``Publishing`` ``Plugins`` ``API`` Improved publishing infrastructure

  * Give providers the ability to specify various custom settings (e.g. via
    forms)
  * Enable publishing through Kadi4Mat via the API
  * Possibly save information like the DOI of a publication automatically

* ``UI`` Improved form generation and validation

  * Form fields should be generated in a more consistent and generic way if
    possible, including (dynamic) custom fields
  * For improved usability, more aspects of forms should be additionally
    validated on the client-side

* ``Files`` ``UI`` ``Plugins`` Improved file previews in the web interface

  * Add additional preview types for less common file types
  * Some previews should probably be "calculated" once after a file is uploaded
    (in a background task) and be saved for later use

* ``UI`` Improved index page and homepage

  * Provide more easily digestible information and eye catchers on the index
    page and possibly other pages, like screenshots, videos, slideshows, etc.
  * Give logged in users a better overview about current updates as well as
    suggested resources
  * Provide some kind of step-by-step guide to get started more easily

Planned features
----------------

* ``API`` Allow API access for third party applications using OAuth2/OIDC

* ``Architecture`` Decentralized peer-to-peer infrastructure

  * Connecting multiple application instances (e.g. by using some central
    registry) to allow for easier collaborating and transparent data exchange

* ``Architecture`` ``Plugins`` Appstore for tools and possibly plugins

  * Centralized "Appstore" for users to easily and directly provide tools for
    direct use or for use inside a workflow
  * Possibly something similar for plugins as well ("Extension Registry"),
    which could even be installed on a per-user basis if feasible

* ``Architecture`` ``Plugins`` Provide a meta package containing all (publicly
  accessible) first-party plugins

  * This especially would make sense once more features have been implemented
    as plugins or split from the current core functionality

* ``Authorization`` User affiliations

  * Either configurable by users or set automatically depending on the used
    authentication mechanism (e.g. LDAP or Shibboleth)
  * Configurable mapping of affiliations and attributes to specific permissions
    and (system) roles or groups

* ``Authorization`` Access requests

  * Give users the ability to request access to resources
  * Would require making the basic metadata of a resource publicly accessible
    for logged in users, which should be configurable per resource

* ``Authorization`` ``Files`` ``Metadata`` Low-level data/metadata access
  and authorization

  * Allow low-level access to data using low-level authorization mechanisms
    (e.g. unix user and group permissions enhanced with ACLs) and
    authentication protocols (e.g. SSH)
  * Provide an API to query files and their basic metadata as well as their
    access permissions from the high-level layer, while also still being able
    to use it to upload files and edit metadata
  * Possibly provide a way to access metadata stored in the database from the
    low-level layer and still use the same authorization mechanism as for files
    if, while also being able to create new metadata directly on the low-level
    layer
  * This will most likely also require assessing the current roles and
    functionality of the different resources again, i.e. users, groups/roles,
    records and collections

* ``Files`` Custom data type/MIME type registry

  * A central place to register well-known MIME types, either based on file
    extensions and/or magic numbers
  * Either globally or configurable by users

* ``Files`` Use streaming to package and send files of a record to a client
  on the fly, without needing to store the data temporarily

* ``Other`` ``Plugins`` Improved internationalization and localization

  * Unify translations of frontend and backend
  * Preparing all missing strings in the code, templates, etc. for translating
  * Actually translating all strings while making sure the UI does not break
  * Allow plugins to specifiy their own translations in a separate domain
  * Possibly allow translations for generic metadata entries

* ``Other`` Quality management/peer review system for resources

  * Manually using user ratings or comments and letting users set the review
    status
  * Basic quality control should be automated in some way if possible

* ``Plugins`` Provide a project template for easier development of plugins

* ``UI`` Quality of life changes for improved/more efficient usability

  * More autocomplete functionality/suggestions wherever possible (e.g. similar
    to tags)
  * More batch operations
  * More (context aware) help popups/texts/links inside the application itself
  * Better navigation and organization of settings/menus/etc.
  * Using a recommender system could help with some of the above steps or be
    used to suggest similar resources

* ``UI`` Add a testing infrastructure for frontend code

* ``Workflow`` Integrate user-provided tools

  * Tools need a uniform way to specify their parameters and related validation
  * Tools need to be able to access all required input data/metadata and create
    new outputs connected to the respective input
  * Tools need to run with the same permissions as the user, either using
    low-level mechanisms or suitable access tokens via an API
  * Tools need to run inside some container for security and portability
    reasons (e.g. using Docker)
  * Tools could either offer their own (HTTP-)API, making their direct
    invocation and data exchange possible, or indirectly through some kind of
    manager

* ``Workflow`` Integrate user-specific workflows

  * Ability to define workflows using the web interface to make use of
    user-provided tools, interface with devices, etc. in an automated manner
  * Running the workflow using an external workflow manager/engine through a
    suitable API, who also takes care of possibly running tools on different
    (HPC-)systems, also offering the ability to query the status/results of
    each step (and possibly offer user interactions)

* ``Workflow`` Develop a generic interface to be able to integrate different
  devices like laboratory equipment for use in workflows

Ideas
-----

* ``Architecture`` Split some modules into independent microservices, each
  providing an HTTP API and using a single authorization service (and possibly
  other shared services)

* ``Authorization`` Temporary access links for specific resources, giving
  external users access (with configurable duration and the possibility of
  revoking them)

* ``Deployment`` Better and more automated release process, possibly including
  publishing the code on Zenodo as well

* ``Files`` Offer a directory structure for records containing a large amount
  of files

  * This could be done similarly to Amazon S3, by interpreting slashes in file
    names as file/folder separators

* ``Other`` Following users/subscribing to resources (integrated into the
  existing notification system)

* ``Other`` Builtin messaging functionality (integrated into the existing
  notification system)

* ``Other`` ``Plugins`` Split the code base into general functionality
  (Kadi) and more specific functionality (Kadi4X)

* ``Other`` Real time updates for notifications using WebSockets or some
  similar technology, either using something like Flask-SocketIO or using a
  separate, asynchronous web server

* ``Other`` Offline access

  * Provide a local/offline version of the application with synchronization
    mechanisms once connection is established again (e.g. in laboratories
    without internet or even network access)

* ``Other`` Type hinting

  * Start to use type hints consistently across all backend code
  * Use some static type checker tool like mypy

* ``Other`` Ability to save resource as drafts

  * This could then also allow the following for e.g. records: Whenever a new
    record is created, each change to its metadata (and possibly files as well)
    will be saved/updated automatically as a draft, the user can then choose
    when to "finalize" the creation process

* ``Other`` Live editing of files

  * Aside from file previews, some file types could be edited directly using
    the web interface
  * This will require suitable frontend editors as well as backend components
    to save any changes to a file
  * File revisions should probably be implemented before that

* ``Other`` Implement a todo list usable on all pages, allowing each user to
  write down personal notes

* ``Other`` Give users the ability to customize/personalize different settings
  and layouts of the application

* ``Other`` Allow users to set timestamped notes/observations to records and
  possibly other resources, additionally to the existing metadata

* ``Other`` Add support for trusted timestamping to records or other
  resources according to RFC3161

* ``Other`` Allow for a more strict content security policy

  * This would require rewriting/restructuring different aspects of the
    frontend assets pipeline, the assets themselves and also templates

* ``Other`` Add project views

  * Project views can simply be logical groupings of different resources
    (users, groups, records, etc.), allowing easier resource organization and
    management

* ``Other`` ``UI`` Provide a system administrator view to manage/configure
  different aspects of the application directly from the frontend

  * Requires the ability to assign users as system administrator
  * Could also be used to manage users, i.e. (de)activate their accounts

* ``Search`` Index text-based data or other data displaying text (e.g. PDFs)
  for searching

* ``UI`` Decouple frontend code from backend code

  * Instead of server-side rendering, everything could be done on the
    client-side while only interacting with the server through the HTTP API
  * This could then also be implemented as a single-page application (SPA)

* ``UI`` More optimized UI for smaller displays

* ``UI`` Android and iOS apps

  * This could simply be a native wrapper of the existing web interface

* ``UI`` Support for older browsers

  * Would at the very least require transpiling all frontend code (in- and
    outside of templates)

* ``UI`` Provide a complete visualization of linked and related resources

* ``UI`` Get rid of jQuery

  * Migrate to Bootstrap v5 once there is a stable release
  * Replace remaining libraries that depend on jQuery with suitable
    alternatives or custom code
