Plugins
=======

This section describes how plugins can be developed to extend the functionality
of Kadi4Mat without having to touch the main application code. As currently all
first party plugins are part of the main application package, examples can be
found in the :mod:`kadi.plugins` module. This also includes code not
implemented as a separate plugin but still using the plugin hooks.

Developing plugins
------------------

The plugin infrastructure in Kadi4Mat builds upon `pluggy
<https://pluggy.readthedocs.io/en/latest/index.html>`__, a generic and flexible
plugin system. It allows implementing different plugin interface functions,
called hook specifications, which a plugin can choose to implement. Each hook
will be called in certain places in the application flow, which will invoke all
corresponding implementations of that hook. A list of all currently existing
hooks can be found below.

In order for the main application to find a plugin, it has to register itself
using the ``kadi_plugins`` setuptools entrypoint. Each plugin needs to specify
a unique name and the module that contains all hook implementations:

.. code-block:: python3

    entry_points={
        "kadi_plugins": [
            "example=kadi_plugin_example.plugin",
        ],
    },

In this case, the plugin is called ``example``, while the hook implementations
are loaded via the :mod:`kadi_plugin_example.plugin` module.

Plugin hooks
------------

The following hook specifications for use in plugins currently exist:

.. note::

  As long as Kadi4Mat is still in beta, the plugin hooks are considered beta as
  well. Once Kadi4Mat version 1.0+ is released, backwards incompatible changes
  will be documented and deprecated hooks will be marked accordingly. Until
  then, hook specifications might still change between versions.

.. automodule:: kadi.plugins.spec
   :members:
   :show-inheritance:
