#!/usr/bin/env bash
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
set -e

#############
# Variables #
#############

KADI_USER="kadi"
KADI_GROUP="www-data"
KADI_HOME="/opt/kadi"
KADI_CONFIG_FILE="${KADI_HOME}/config/kadi.py"

STORAGE_PATH_DEFAULT="${KADI_HOME}/storage"
MISC_UPLOADS_PATH_DEFAULT="${KADI_HOME}/uploads"

UWSGI_CONFIG_FILE="/etc/uwsgi/apps-available/kadi.ini"
APACHE_CONFIG_FILE="/etc/apache2/sites-available/kadi.conf"

DEFAULT_CERT_FILE="/etc/ssl/certs/kadi.crt"
DEFAULT_KEY_FILE="/etc/ssl/private/kadi.key"

DB_NAME="kadi"
DB_USER="kadi"
DB_PW="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c16)"

#############
# Functions #
#############

info() {
  echo ""
  tput bold
  echo "$1"
  tput sgr0
}

success() {
  echo ""
  tput bold
  tput setaf 2
  echo "$1"
  tput sgr0
}

kadi() {
  sudo -H -u $KADI_USER bash -c "source ~/.profile && $1"
}

postgres() {
  sudo -u postgres psql -c "$1" > /dev/null
}

config() {
  echo "$1" >> $KADI_CONFIG_FILE
}

########
# Main #
########

setup() {

if [[ $EUID > 0 ]]; then
  echo "This script must be run as root."
  exit 1
fi


info "Adding the Elasticsearch repository..."

ES_SOURCE_LIST="/etc/apt/sources.list.d/elastic-7.x.list"

if [[ ! -f $ES_SOURCE_LIST ]]; then
  apt install -y wget apt-transport-https gnupg
  wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
  echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" >> $ES_SOURCE_LIST
  echo "Repository added."
else
  echo "Repository already exists."
fi


info "Installing dependencies..."

apt update
apt install -y openssl libmagic1 python3 virtualenv postgresql redis-server elasticsearch \
               uwsgi uwsgi-plugin-python3 apache2 libapache2-mod-proxy-uwsgi libapache2-mod-xsendfile


info "Enabling and starting Elasticsearch..."

systemctl enable elasticsearch.service
systemctl start elasticsearch.service


info "Setting up '${KADI_USER}' user..."

if ! id $KADI_USER &> /dev/null; then
  adduser $KADI_USER --system --home $KADI_HOME --ingroup $KADI_GROUP --shell /bin/bash
  sudo -H -u $KADI_USER bash -c 'echo "export KADI_CONFIG_FILE=\${HOME}/config/kadi.py" >> ~/.profile'
  sudo -H -u $KADI_USER bash -c 'echo "test -z \"\${VIRTUAL_ENV}\" && source \${HOME}/venv/bin/activate" >> ~/.profile'
else
  echo "User already exists."
fi


info "Setting up virtual environment..."

if [[ ! -d "${KADI_HOME}/venv" ]]; then
  sudo -H -u $KADI_USER bash -c 'virtualenv -p python3 ${HOME}/venv'
else
  echo "Virtual environment already exists."
fi


info "Installing kadi..."

kadi 'pip install kadi'


info "Setting up database user '${DB_USER}'..."

if [[ ! $(sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='${DB_USER}'") ]]; then
  postgres "CREATE USER ${DB_USER} WITH PASSWORD '${DB_PW}';"
  echo "Database user created."
else
  DB_EXISTS=true
  echo "Database user already exists."
fi


info "Setting up database '${DB_NAME}'..."

if [[ ! $(sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_database WHERE datname='${DB_NAME}'") ]]; then
  postgres "CREATE DATABASE ${DB_NAME} OWNER ${DB_USER} ENCODING 'UTF-8';"
  echo "Database created."
else
  echo "Database already exists."
fi


info "Creating Kadi configuration file..."

if [[ ! -f $KADI_CONFIG_FILE ]]; then
  while [[ -z $SERVER_NAME ]]; do
    read -p "Enter the name or IP of the host: " SERVER_NAME
  done

  read -p "Enter the path for storing data [${STORAGE_PATH_DEFAULT}]: " STORAGE_PATH
  [[ -z $STORAGE_PATH ]] && STORAGE_PATH=$STORAGE_PATH_DEFAULT

  mkdir -p $STORAGE_PATH
  chown ${KADI_USER}:${KADI_GROUP} $STORAGE_PATH
  chmod 750 $STORAGE_PATH

  read -p "Enter the path for storing miscellaneous uploads (e.g. profile or group pictures) [${MISC_UPLOADS_PATH_DEFAULT}]: " MISC_UPLOADS_PATH
  [[ -z $MISC_UPLOADS_PATH ]] && MISC_UPLOADS_PATH=$MISC_UPLOADS_PATH_DEFAULT

  mkdir -p $MISC_UPLOADS_PATH
  chown ${KADI_USER}:${KADI_GROUP} $MISC_UPLOADS_PATH
  chmod 750 $MISC_UPLOADS_PATH

  if [[ -n $DB_EXISTS ]]; then
    postgres "ALTER USER ${DB_USER} WITH PASSWORD '${DB_PW}';"
  fi

  kadi 'mkdir -p ${HOME}/config'
  kadi "touch ${KADI_CONFIG_FILE}"
  kadi "chmod 640 ${KADI_CONFIG_FILE}"

  config "import kadi.lib.constants as const"
  config "AUTH_PROVIDERS = [{\"type\": \"local\"}]"
  config "FOOTER_NAV_ITEMS = []"
  config "MAIL_ERROR_LOGS = []"
  config "MAIL_NO_REPLY = \"no-reply@${SERVER_NAME}\""
  config "MAX_UPLOAD_SIZE = const.ONE_GB"
  config "MAX_UPLOAD_USER_QUOTA = 10 * const.ONE_GB"
  config "MISC_UPLOADS_PATH = \"${MISC_UPLOADS_PATH}\""
  config "PLUGIN_CONFIG = {}"
  config "PLUGINS = []"
  config "SECRET_KEY = \"$(kadi 'kadi utils secret-key')\""
  config "SENTRY_DSN = None"
  config "SERVER_NAME = \"${SERVER_NAME}\""
  config "SMTP_HOST = \"localhost\""
  config "SMTP_PASSWORD = \"\""
  config "SMTP_PORT = 25"
  config "SMTP_USE_TLS = False"
  config "SMTP_USERNAME = \"\""
  config "SQLALCHEMY_DATABASE_URI = \"postgresql://${DB_USER}:${DB_PW}@localhost/${DB_NAME}\""
  config "STORAGE_PATH = \"${STORAGE_PATH}\""

  echo "Kadi configuration file created."
else
  echo "Kadi configuration file already exists."
fi


info "Configuring uwsgi..."

if [[ ! -f "$UWSGI_CONFIG_FILE" ]]; then
  kadi "kadi utils uwsgi --default --out ${KADI_HOME}/kadi.ini"
  mv ${KADI_HOME}/kadi.ini /etc/uwsgi/apps-available/
  ln -s $UWSGI_CONFIG_FILE /etc/uwsgi/apps-enabled/
else
  echo "uwsgi is already configured."
fi


info "Configuring Apache..."

if [[ ! -f "$APACHE_CONFIG_FILE" ]]; then
  while [[ -z $SERVER_NAME ]]; do
    read -p "Enter the name or IP of the host: " SERVER_NAME
  done

  openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout $DEFAULT_KEY_FILE -out $DEFAULT_CERT_FILE \
              -subj "/CN=${SERVER_NAME}" -addext "subjectAltName=DNS:${SERVER_NAME}"

  kadi "kadi utils apache --default --out ${KADI_HOME}/kadi.conf"
  mv ${KADI_HOME}/kadi.conf /etc/apache2/sites-available/

  a2dissite 000-default
  a2ensite kadi
  a2enmod proxy_uwsgi ssl xsendfile headers
else
  echo "Apache is already configured."
fi


info "Configuring Celery and Celery beat..."

if [[ ! -f "/etc/systemd/system/kadi-celery.service" ]]; then
  kadi "kadi utils celery --default --out ${KADI_HOME}/kadi-celery.service"
  kadi "kadi utils celerybeat --default --out ${KADI_HOME}/kadi-celerybeat.service"

  mv ${KADI_HOME}/kadi-celery.service /etc/systemd/system/
  mv ${KADI_HOME}/kadi-celerybeat.service /etc/systemd/system/

  mkdir /var/log/celery
  chown ${KADI_USER}:${KADI_GROUP} /var/log/celery

  echo "d /run/celery 0755 $KADI_USER $KADI_GROUP" > /usr/lib/tmpfiles.d/celery.conf
  systemd-tmpfiles --create

  systemctl daemon-reload
  systemctl enable kadi-celery kadi-celerybeat
else
  echo "Celery is already configured."
fi


info "Setting up Kadi..."

kadi 'kadi db init'
kadi 'kadi search init'


info "Restarting services..."

systemctl restart apache2 uwsgi kadi-celery kadi-celerybeat
echo "Restarted services."


success "Setup finished successfully!"
echo "Note that the web server is currently using a self-signed certificate, which should only be used internally or for testing."
echo "Once you want to use another certificate, make sure to adapt the corresponding values in the Apache configuration file ('${APACHE_CONFIG_FILE}')."
echo "The generated certificate file ('${DEFAULT_CERT_FILE}') and key file ('${DEFAULT_KEY_FILE}') can be safely deleted afterwards."
echo "Also, please recheck and, if necessary, adapt the Kadi ('${KADI_CONFIG_FILE}') and uwsgi ('${UWSGI_CONFIG_FILE}') configuration files."

}

setup
